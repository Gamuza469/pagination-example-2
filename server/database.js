const knex = require('knex');

const initDb = async () => {
    const database = knex({
        client: 'sqlite3',
        connection: ':memory:',
        useNullAsDefault: true
    });

    await database.schema.createTable('comments', table => {
        table.integer('id').primary();
        table.integer('postId').unsigned().notNullable();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('body').notNullable();
        table.unique(['id', 'postId']);
    });

    return database;
};

module.exports = initDb;
