
const getPagedComments = async (database, commentsPerPage, pageToGet) => {
    /* Get total comment count */
    const [ countResult ] = await database('comments').count({ totalCount: '*' });
    const { totalCount } = countResult;

    /* Get page count */
    const offset = commentsPerPage * ( pageToGet - 1 ); 
    const pagedComments = await database('comments').offset(offset).limit(commentsPerPage);

    let availablePages = Math.trunc(totalCount / commentsPerPage);
    
    /* Add extra page if needed */
    if ( totalCount % commentsPerPage ) {
        availablePages++;
    }

    console.log(pagedComments);
    console.log('Total comment count: ', totalCount);
    console.log('Total pages to browse: ', availablePages);
    console.log('Current selected page: ', pageToGet);
    console.log('Selected page comment count: ', pagedComments.length);
};

module.exports = getPagedComments;
