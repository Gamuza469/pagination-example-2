# Pagination Example N°2

This is a pagination example with shows how pagination should be done on the back-end server. Which data the user should send and which data should the server respond with.

## Usage

1. Install all dependencies using `npm install`.
2. Run the `npm start` command.
3. Change the `PAGE_TO_GET` and `COMMENTS_PER_PAGE` constants @ `index.js` and see the results by running `npm start`.

## Short explanation

The user should send `PAGE_TO_GET` and `COMMENTS_PER_PAGE` data to the server alongside the request to fetch resources. The server then has to respond with the following data:

- Total element count from the collection. That means the entity count before paginate the results.
- Total pages to browse. This means how many pages will span all the results.
- Hoy many entities have to be shown at maximum per page.
- The page the user has selected to view.
- The entity count for the current selected page. If there is only one page or the selected page is the last then this count may not be equal to the element count to be shown on a page.

This section will be explained throughly at a later time.
