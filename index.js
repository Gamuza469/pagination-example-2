const initDb = require('./server/database');
const getComments = require('./server/getComments');

const execute = async () => {
    /* Exercise setup */
    const database = await initDb();
    const allComments = await getComments();

    await database('comments').insert(allComments);

    /* Parameters (from client side) */
    const PAGE_TO_GET = 17;
    const COMMENTS_PER_PAGE = 30;

    /* Get total comment count */
    const [ countResult ] = await database('comments').count({ totalCount: '*' });
    const { totalCount } = countResult;

    let availablePages = Math.trunc(totalCount / COMMENTS_PER_PAGE);

    /* Add extra page if needed */
    if ( totalCount % COMMENTS_PER_PAGE ) {
        availablePages++;
    }

    /* Get paged comments */
    const offset = COMMENTS_PER_PAGE * ( PAGE_TO_GET - 1 );
    const pagedComments = await database('comments').offset(offset).limit(COMMENTS_PER_PAGE);

    console.log(pagedComments);
    console.log('Total comment count: ', totalCount);
    console.log('Total pages to browse: ', availablePages);
    console.log('Comments per page: ', COMMENTS_PER_PAGE);
    console.log('Current selected page: ', PAGE_TO_GET);
    console.log('Selected page comment count: ', pagedComments.length);

    /* Close database connection */
    database.destroy();
};

execute();
